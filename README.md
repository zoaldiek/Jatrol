Jatrol
======

Jatrol is a program that brings together tools to improve pytrol. He's splitting in half:
  - Guitrol: allows the graphical display of the simulation.
  - Statrol: allows you to display the statistics of the parameters as a function of the various simulations.




How to Use Pytrol Gui
===================


Launch the Gui:
-------------

The gui has been written in Java, so you have to found the Jar called Gui.
It is situated in the Pytrol Projet at the root folder.

To launch it, open a console and write the next command : 

 *  `` java -jar jatrol.jar -g <config-file> <log-file>``
 
 * Where the **config-file** is file which contains the configuration of the simulation that you want display.  That kind of file is communly stored in the following folder : */various/sim_materials/config/FILE_NAME.json*
 You have to specify all the path since the root folder of the project.
 
 * And the **log-file** is the file which contains all the logs of the simulation already executed stored in the folder : */various/logs/LOG_FILE.log.json*
 
 This command will launch a GraphStream window displaying the graph of the simulation. GraphStream uses a force layout algorithm to position the nodes so wait few seconds in order to have a graph with a correct strucutre.

 
Bindings :
-------------
By default the step is 1.

* Z : Step forward the simulation according to the current step.
* S : Step down the simulation according to the current step.
* D : Increase the step by multipying it by 10.
* Q : Decrease the step by dividing it by 10.
* G : Let the simulation step forward by itself.


-------

How to Use Pytrol Stat
=====================


To launch it, open a console and write the next command : 

 *  `` java -jar jatrol.jar -s configuration.json``
 
 * Where the **configuration.json** is file which contains parameters of the statrol program that you want display.  
 You have to specify all the path since the root folder of the project.
 You have to specify between **graph** and **histo**.

Important: It is essential that there is no other file than the simulation logs in the folders browsed.




